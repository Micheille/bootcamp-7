import { makeStyles } from '@material-ui/core/styles';

const useAppStyles = makeStyles(() => ({
  app: {
    height: '100vh',
  },
}));

export default useAppStyles;
