import { createMuiTheme } from '@material-ui/core';

const theme = createMuiTheme({
  spacing: 4,

  breakpoints: {
    values: {
      sm: 700,
    },
  },

  palette: {
    primary: {
      main: '#0F4780',
    },
    secondary: {
      main: '#6B64FF',
    },
    background: {
      default: 'lightsteelblue',
    },
  },

  typography: {
    button: {
      fontSize: '16px',
      fontWeight: 'bold',
    },
  },

  overrides: {
    MuiButton: {
      root: {
        width: '100%',
        padding: '10px 16px',
        transition: '0.2s all ease-in',
      },
      containedSecondary: {
        '&:hover': {
          opacity: '0%',
          transition: '0.2s all ease-out',
        },
      },
    },

    MuiPaper: {
      root: {
        width: '400px',
        padding: '20px',
      },
      rounded: {
        borderRadius: '8px',
      },
    },
  },
});

export default theme;
