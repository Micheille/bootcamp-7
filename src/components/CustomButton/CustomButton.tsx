import React, { ComponentProps } from 'react';
import { Button } from '@material-ui/core';
import clsx from 'clsx';

import useButtonStyles from './CustomButton.styles';

type muiButtonProps = ComponentProps<typeof Button>;

function CustomButton({
  children,
  className = '',
  error,
  ...restProps
}: {
  children?: string;
  className?: string;
  error?: boolean;
  restProps?: muiButtonProps;
} & muiButtonProps) {
  const classes = useButtonStyles({ error });

  return (
    <Button
      className={clsx(className, error && classes.error)}
      variant="contained"
      {...restProps}
    >
      {children}
    </Button>
  );
}

export default CustomButton;
