import { makeStyles } from '@material-ui/core/styles';

import theme from '../../theme/muiTheme';

type propsType = {
  error?: boolean;
};

const useButtonStyles = makeStyles(() => ({
  error: {
    backgroundColor: (props: propsType) =>
      props.error ? theme.palette.error.main : 'initial',
    color: theme.palette.common.white,
    '&:hover': { backgroundColor: theme.palette.error.dark },
  },
}));

export default useButtonStyles;
