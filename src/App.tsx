import React from 'react';
import { ThemeProvider } from '@material-ui/core/styles';
import { CssBaseline, Grid, Button, Paper } from '@material-ui/core';

import theme from './theme/muiTheme';
import { CustomButton } from './components/CustomButton';
import useAppStyles from './App.styles';

import '@fontsource/roboto/300.css';
import '@fontsource/roboto/400.css';
import '@fontsource/roboto/500.css';
import '@fontsource/roboto/700.css';

function App() {
  const classes = useAppStyles();

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <Grid
        container
        className={classes.app}
        justify="center"
        alignItems="center"
      >
        <Paper>
          <Grid container spacing={4}>
            <Grid item xs={12} sm={6}>
              <Button variant="contained" color="primary">
                Primary
              </Button>
            </Grid>

            <Grid item xs={12} sm={6}>
              <Button variant="contained" color="secondary">
                Shy button
              </Button>
            </Grid>

            <Grid item xs={12} sm={6}>
              <CustomButton error>Error button</CustomButton>
            </Grid>

            <Grid item xs={12} sm={6}>
              <CustomButton>Just a button</CustomButton>
            </Grid>
          </Grid>
        </Paper>
      </Grid>
    </ThemeProvider>
  );
}

export default App;
